
import datetime

# FILES
# file_handler -- this is file handler
# we will use this file_handler to read, write and nevigate in the file
file_handler = open('c:/itay/hello.txt') # open file for read - default

print(file_handler)

print(file_handler.tell()) # tell me where i am in the file

listOfLines = file_handler.readlines() # read all the lines into a list
                             # not for very large files

#'abc'.replace('a', 'd') --> 'dbc'
#'abc'.replace('0','1') --> 'abc'

print('sum=',sum([len(word) for word in listOfLines]))
print([len(word) for word in listOfLines])
print(listOfLines)
print([word.replace('\n', '') for word in listOfLines])
print(file_handler.tell())
print('========================================')

file_handler.seek(0)

#listOfLines = file_handler.readlines() # read all the lines into a list

while file_handler.tell() < 44:
    line = file_handler.readline()
    print(line, end="")

file_handler.close()

f1 = open('c:/itay/hello.txt')
for line in f1:
    print(line, end="")
f1.close()

with open('c:/itay/hello.txt') as f1:
    for line in f1:
        print(line, end="")
    f1.seek(0)
    # because of the with --> here the open will be closed
    # f1.close()

# using w if file not exist -- will be created
# w lets me move inside the file and update
with open('c:/itay/new1.txt', 'w') as f1:
    f1.write(f'Hello new file!! {datetime.datetime.now()}' )
    f1.seek(0)
    f1.write('bla bla')
    # cannot read when opened with w -- crash!!!
    #f1.seek(0)
    #line = f1.readline()
    #print(f'line = {line}')

print()
# using a if file not exist -- will be created
with open('c:/itay/new2.txt', 'a') as f1:
    f1.seek(0)
    print(f1.tell())
    f1.write(f'\nHello new file!! {datetime.datetime.now()}' )

with open('c:/itay/new1.txt', 'w+') as f1:
    f1.write(f'Hello new file!! {datetime.datetime.now()}' )
    f1.seek(0)
    f1.write('bla bla')
    # can read when opened with w
    f1.seek(0)
    line = f1.readline()
    print(f'line read in w+ = {line}')

# can also append to file
with open('c:/itay/new1.txt', 'r+') as f1:
    f1.write(f'0')
    f1.seek(0)
    line = f1.readline()
    print(f'line read in r+ = {line}')
    f1.write(f'\nfirst line in file {datetime.datetime.now()}' )
    f1.seek(0,2) # -- move to end of file!
    f1.write(f'\nadd this line in EOF {datetime.datetime.now()}' )

# can also append to file
with open('c:/itay/new3.txt', 'a+') as f1:
    f1.write(f'\n[1]this in end of file {datetime.datetime.now()}')
    f1.write(f'\n[2]first line in file {datetime.datetime.now()}' )
    f1.seek(0)
    line = f1.readline()
    print(f'91 = {f1.tell()}')
    print(f'line read in a+ = {line}')
    f1.seek(0, 2)  # -- move to end of file!
    f1.write(f'\n[3]add this line in EOF {datetime.datetime.now()}' )


f1 = open('c:/itay/new2.txt', 'r')
for line_one in open('c:/itay/new2.txt', 'r'): #.readlines():
    print(line_one)

with open('c:/itay/new2.txt', 'w') as f1:
    with open('c:/itay/hello.txt', 'r') as f2:
        for line in f2.readlines():
            f1.write(line)

def foo_2():
    return 2,3

a,b = [3,4]
a,b = [b,a]
print(f'a = {a}')
print(f'b = {b}')

x,y = foo_2()
print(x)
print(y)
z = foo_2()
print(z)