# create dictionary with duplicated values
# 1 : 'o' one
# 2 : 't' two
# 3 : 't'
# 4 : 'f'
# 5 : 'f'
# ... 10

# write a loop to display how many times
# each letter repeats itself

lettersDict = { 1: 'o', 2: 't', 3: 't', 4: 'f', 5: 'f', 6: 's', 7: 's', 8: 'e', 9: 'n', 10: 't'}
print(lettersDict)
print(f'values without set = {list(lettersDict.values())}')
set_keys_dict = set(lettersDict.values())
print(set_keys_dict)

dic_letters_count = dict()
for n in set_keys_dict:
    dic_letters_count[n] = 0
print(dic_letters_count)
# {'s': 0, 'o': 0, 'e': 0, 'f': 0, 'n': 0, 't': 0}

for v in list(lettersDict.values()):
    count = dic_letters_count[v]
    dic_letters_count[v] = count + 1

print(dic_letters_count)